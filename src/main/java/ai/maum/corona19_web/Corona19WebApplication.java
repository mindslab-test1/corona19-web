package ai.maum.corona19_web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Corona19WebApplication {

    public static void main(String[] args) {
        SpringApplication.run(Corona19WebApplication.class, args);
    }

}
