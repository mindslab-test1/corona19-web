package ai.maum.corona19_web.RestAPI.controller;

import ai.maum.corona19_web.RestAPI.model.InfectedPersonVO;
import ai.maum.corona19_web.RestAPI.model.InfectedRelationVO;
import ai.maum.corona19_web.RestAPI.service.InfectedPersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


@Slf4j
@RestController
public class InfectedPersonController {

    @Autowired
    InfectedPersonService service;

    @GetMapping("/corona/api/infected-persons/addresses")
    public List<String> getAddressList(HttpServletRequest request, HttpServletResponse response) {

        return service.getAddressList();
    }

    @GetMapping("/corona/api/infected-persons/relations")
    public List<InfectedRelationVO> getRelationList(HttpServletRequest request,
                                                    HttpServletResponse response,
                                                    String start_date,
                                                    String end_date,
                                                    String start_time,
                                                    String end_time,
                                                    String address,
                                                    Integer person_id) {

        log.info("# InfectedPersonController.getRelationList() >> start_date={} / end_date = {} / start_time={} / end_time={} / address={} / person_id={}", start_date, end_date, start_time, end_time, address, person_id);
        return service.getRelationList(start_date, end_date, start_time, end_time, address, person_id);
    }

}