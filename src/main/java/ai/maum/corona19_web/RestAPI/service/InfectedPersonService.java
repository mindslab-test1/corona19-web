package ai.maum.corona19_web.RestAPI.service;

import ai.maum.corona19_web.RestAPI.model.InfectedRelationVO;
import ai.maum.corona19_web.mapper.InfectedPerson_Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InfectedPersonService {

    @Autowired
    InfectedPerson_Mapper mapper;


    public List<String> getAddressList() {
        return mapper.getAddressList();
    }

    public List<InfectedRelationVO> getRelationList(String start_date, String end_date, String start_time, String end_time, String address, Integer person_id) {
        return mapper.getRelationList(start_date, end_date, start_time, end_time, address, person_id);
    }
}
