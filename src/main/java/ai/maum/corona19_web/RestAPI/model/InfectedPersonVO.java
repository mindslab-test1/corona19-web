package ai.maum.corona19_web.RestAPI.model;

import lombok.Data;

@Data
public class InfectedPersonVO {
    private int id;
    private String name;
}
