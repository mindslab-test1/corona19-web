package ai.maum.corona19_web.RestAPI.model;

import lombok.Data;

@Data
public class InfectedRelationVO {
    private String date;
    private String least_start_time;
    private String greatest_end_time;
    private String addr;
    private String distance;

    private int a_person_id;
    private String a_name;
    private String a_confirm_date;
    private String a_start_time;
    private String a_end_time;
    private String a_lat;
    private String a_lon;

    private int b_person_id;
    private String b_name;
    private String b_confirm_date;
    private String b_start_time;
    private String b_end_time;
    private String b_lat;
    private String b_lon;
}
