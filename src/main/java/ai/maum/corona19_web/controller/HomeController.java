package ai.maum.corona19_web.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 메인 컨트롤러
 *
 * @author unongko
 * @version 1.0
 */

@Slf4j
@Controller
public class HomeController {

    @GetMapping("/")
    public ModelAndView demo1(HttpServletRequest request, HttpServletResponse response, Model model) {
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("index");
        return modelAndView;
    }

    @GetMapping("/infected-relation")
    public ModelAndView home(HttpServletRequest request, HttpServletResponse response, Model model) {
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("path");
        return modelAndView;
    }

}