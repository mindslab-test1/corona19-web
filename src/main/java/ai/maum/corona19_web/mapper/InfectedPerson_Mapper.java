package ai.maum.corona19_web.mapper;

import ai.maum.corona19_web.RestAPI.model.InfectedRelationVO;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

@Repository
public class InfectedPerson_Mapper {

    @Autowired
    @Qualifier("sqlSession")
    private SqlSessionTemplate sqlSession;

    private static final String NAMESPACE = "ai.maum.corona19_web.mapper.InfectedPerson_Mapper";

    public List<String> getAddressList() {
        return sqlSession.selectList(NAMESPACE + ".getAddressList");
    }

    public List<InfectedRelationVO> getRelationList(String start_date, String end_date, String start_time, String end_time, String address, Integer person_id) {
        HashMap param = new HashMap<String, Object>();
        param.put("start_date", (start_date.length() != 0 ? start_date : null));
        param.put("end_date", (end_date.length() != 0 ? end_date : null));
        param.put("start_time", (start_time.length() != 0 ? start_time : null));
        param.put("end_time", (end_time.length() != 0 ? end_time : null));
        param.put("address", (address.length() != 0 ? address + '%' : null));
        param.put("person_id", person_id);
        return sqlSession.selectList(NAMESPACE + ".getRelationList", param);
    }
}
