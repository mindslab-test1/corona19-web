$(document).ready(function (){
   loadAddressList();
});



/* ===================================================================================================================== */
// 주소 관리
/* ===================================================================================================================== */

/* 검색 조건에서 사용할 주소 목록 조회 */
function loadAddressList() {
    $.ajax({
        url: '/corona/api/infected-persons/addresses',
        type: 'GET'
    }) .done((data, textStatus, jqXHR) => {
        console.log("성공: ", data[2]);
        console.dir(data);

        insertAddress(data);

    }) .fail((jqXHR, textStatus, errorThrown) => {
        console.log("실패");
    }) ;
}

function insertAddress(addr_list) {
    for(var xx = 0; xx < addr_list.length; xx++){
        var option = $("<option>"+addr_list[xx]+"</option>");
        $('#Select_Address').append(option);
    }
}



/* ===================================================================================================================== */
// 날짜 관리
/* ===================================================================================================================== */

function isValidDate(date) {
    var datatimeRegexp = /^(19|20)\d{2}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[0-1])$/

    if ( !datatimeRegexp.test(date) ) {
        alert("날짜는 yyyy-mm-dd 형식으로 입력해주세요.");
        return false;
    }

    return true;
}

function isValidPeriodDate(startDate, endDate) {
    var startDateArr = startDate.split('-');
    var endDateArr = endDate.split('-');

    var startDateCompare = new Date(startDateArr[0], parseInt(startDateArr[1])-1, startDateArr[2]);
    var endDateCompare = new Date(endDateArr[0], parseInt(endDateArr[1])-1, endDateArr[2]);

    if(startDateCompare.getTime() > endDateCompare.getTime()) {
        alert("시작날짜와 종료날짜를 확인해 주세요.");
        return false;
    }

    return true;
}

function checkDate() {
    if($('#startDate').val().length > 0 && isValidDate($('#startDate').val()) == false) return;
    if($('#endDate').val().length > 0 && isValidDate($('#endDate').val()) == false) return;

    if($('#startDate').val().length > 0 && $('#endDate').val().length && isValidPeriodDate($('#startDate').val(), $('#endDate').val()) == false) return;

    if($('#startDate').val().length == 0 && $('#Select_StartTime option:selected').val().length > 0) {
        alert("시간 지정은 날짜를 함께 지정해야 합니다.");
        return;
    }

    if($('#endDate').val().length == 0 && $('#Select_EndTime option:selected').val().length > 0) {
        alert("시간 지정은 날짜를 함께 지정해야 합니다.");
        return;
    }

    if($('#Select_StartTime option:selected').val().length > 0 && $('#Select_EndTime option:selected').val().length > 0) {
        if($('#startDate').val() == $('#endDate').val() && $('#Select_EndTime option:selected').val() < $('#Select_StartTime option:selected').val()) {
            alert("시작날짜의 시간과 종료날짜의 시간을 확인하세요.");
            return;
        }
    }

    // console.log("startDate: [", $('#startDate').val(), "] length=",  $('#startDate').val().length);
    // console.log("endDate: [", $('#endDate').val(), "] length=",  $('#endDate').val().length);
}


/* ===================================================================================================================== */
// 검색
/* ===================================================================================================================== */

$('#serchBtn').on('click',function() {
    checkDate();

    $('#resultField').fadeIn();

    var offset = $('#resultField').offset();
    $('html').animate({scrollTop : offset.top}, 400);

    loadInfectedRelationList();
});

function loadInfectedRelationList() {
    let infected_person_id = $('#Input_InfectedPersonId').val();
    infected_person_id = (infected_person_id.length == 0 ? null : infected_person_id);

    let address = $('#Select_Address option:selected').val();
    address = (address.length == 0 ? null : address);

    let start_date = $('#startDate').val();
    start_date = (start_date.length == 0 ? null : start_date);

    let end_date = $('#endDate').val();
    end_date = (end_date.length == 0 ? null : end_date);

    let start_time = $('#Select_StartTime option:selected').val();
    start_time = (start_time.length == 0 ? null : start_time);

    let end_time = $('#Select_EndTime option:selected').val();
    end_time = (end_time.length == 0 ? null : end_time);

    console.log("address = ", address);
    $.ajax({
        url: '/corona/api/infected-persons/relations',
        type: 'GET',
        dataType : "json",
        data: {
            start_date: start_date,
            end_date: end_date,
            start_time: start_time,
            end_time: end_time,
            address: address,
            person_id: infected_person_id
        }
    }) .done((data, textStatus, jqXHR) => {
        console.log("성공");
        console.dir(data);

        insertRelations(data);

    }) .fail((jqXHR, textStatus, errorThrown) => {
        console.log("실패");
    }) ;
}



/* ===================================================================================================================== */
// 역학 관계
/* ===================================================================================================================== */

function insertRelations(relation_list) {
    $('#Table_Body').empty();
    for(let xx = 0; xx < relation_list.length; xx++) {
        insertRelation_Row(xx, relation_list[xx]);
    }

    //상세보기
    $('.dtl_btn').on('click',function() {
        console.log('dkdkdk');
        let state = $(this).children('span').is(':visible');
        $('.dtl_btn').children('span').hide();

        if(state){
            $(this).children('span').fadeOut();
        }else{
            $(this).children('span').fadeIn();
        }
    })
}

function insertRelation_Row(row, relation) {
    let format = '\
        <tr>\
            <td scope="row">' +  relation.date + '</td>\
            <td>' + relation.least_start_time + ' ~ ' + relation.greatest_end_time + '</td>\
            <td>\
                ' + relation.addr + '\
                <span>( ' + relation.a_lat + ' / ' + relation.a_lon + ' )</span>\
            </td>\
            <td class="overlap" id="Row_' + row + '">\
            </td>\
        </tr>\
        ';

    $('#Table_Body').append(format);

    insertRelation_SubRow(row, 0);
    insertRelation_Col(row, 0, relation.a_person_id, relation.a_confirm_date, relation.a_start_time, relation.addr, relation.a_lat, relation.a_lon);
    insertRelation_Col(row, 0, relation.b_person_id, relation.b_confirm_date, relation.b_start_time, relation.addr, relation.b_lat, relation.b_lon);
}

function insertRelation_SubRow(row, sub_row) {
    let format = '\
        <ul id="SubRow_' + row + '_' + sub_row + '">\
        </ul>\
    ';

    $('#Row_' + row).append(format);
}

function insertRelation_Col(row, sub_row, persion_id, confirm_date, start_time, addr, lat, lon) {
    let simple_date = confirm_date.substring(5).replace('-', '/');

    let format = '\
        <li>\
            #' + persion_id + ' (확진: ' + simple_date + ')\
            <div class="dtl_btn">\
                <img src="resources/images/search.png" alt="상세보기 이미지">\
                <span><em>' + start_time  + '</em> ' + addr + ' ( ' + lat + ' / ' + lon + ')</span>\
            </div>\
        </li>\
    ';

    $('#SubRow_' + row + '_' + sub_row).append(format);
}